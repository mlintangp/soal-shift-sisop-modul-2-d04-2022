#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>


void list_hewan_dir_air(char *basePath)
{
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if (!dir)
        	return;

	FILE *fptr;
	char fname[150];
	strcpy(fname, basePath);
	strcat(fname, "list.txt");

	fptr = fopen(fname, "a+");


	while ((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
		{
			char list_name[150] = "";

			//file ownership (user)
			struct stat info;
			char ownership_name[100] = "";
			int r1;

			char temp1[100] = "";
			strcpy(temp1, basePath);
                        strcat(temp1, dp->d_name);
			r1 = stat(temp1, &info);
			if( r1==-1 )
			{
				fprintf(stderr,"File error\n");
				exit(1);
			}

			struct passwd *pw = getpwuid(info.st_uid);
			struct group  *gr = getgrgid(info.st_gid);

			if (pw != 0) strcat(ownership_name, pw->pw_name);

			strcat(list_name, ownership_name);
			strcat(list_name, "_");

			//file permission (owner)
			struct stat fs;
			char permission_name[100] = "";
			int r2;

			char temp2[100] = "";
			strcpy(temp2, basePath);
			strcat(temp2, dp->d_name);
			r2 = stat(temp2,&fs);
			if( r2==-1 )
			{
				fprintf(stderr,"File error\n");
				exit(1);
			}

			if( fs.st_mode & S_IRUSR ) {
				strcat(permission_name, "r");
			}
			if( fs.st_mode & S_IWUSR ) {
				strcat(permission_name, "w");
			}
			if( fs.st_mode & S_IXUSR ) {
				strcat(permission_name, "x");
			}


			strcat(list_name, permission_name);
			strcat(list_name, "_");

			if (!(strstr(dp->d_name, "txt"))) {
				strcat(list_name, dp->d_name);
                                fprintf(fptr, "%s\n", list_name);
                        }
		}
	}

	closedir(dir);
	fclose(fptr);
}


void make_folder(char *folder_path) {

	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

	if (child_id == 0) {
                char *argv[] = {"mkdir", "-p", folder_path, NULL};
                execv("/usr/bin/mkdir", argv);

        } else {
                while ((wait(&status)) > 0);
	}

}

//download from my google drive
//wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1hR0qFnGeMnAgEwNUYiNt4LKcf6TT2EGx' -O /home/lintang/modul2/animal.zip

void download_animal_zipfile() {
	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
		char link[250] = "https://docs.google.com/uc?export=download&id=1hR0qFnGeMnAgEwNUYiNt4LKcf6TT2EGx";
		char *argv[] = {"wget", "--no-check-certificate", link, "-O", "/home/lintang/modul2/animal.zip", NULL};
		execv("/usr/bin/wget", argv);
	} else {
		while ((wait(&status)) > 0);
	}
}

//unzip animal.zip
void unzip_animal() {
	pid_t child_id;
        int status;
	child_id = fork();

	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
		char *argv[] = {"unzip", "/home/lintang/modul2/animal.zip", "-d", "/home/lintang/modul2/", NULL};
		execv("/usr/bin/unzip", argv);
	} else {
		while ((wait(&status)) > 0);
	}
}

//folder hewan darat
void folder_darat(char *filename, char *path_dir_darat) {
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
		char *argv[] = {"mv", filename, path_dir_darat, NULL};
		execv("/usr/bin/mv", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

//folder hewan air
void folder_air(char *filename, char *path_dir_air) {
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"mv", filename, path_dir_air, NULL};
                execv("/usr/bin/mv", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

//hapus file bukan darat/air
void delete_hewan(char *filename) {
        pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"rm", filename, NULL};
		execv("/usr/bin/rm", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}


//list.txt
void list_file() {
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"touch", "/home/lintang/modul2/air/list.txt", NULL};
                execv("/usr/bin/touch", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}


int main() {

	DIR *dp;
	struct dirent *ep;
	char path_dir_modul2[50] = "/home/lintang/modul2/";
	char path_dir_animal[100] = "/home/lintang/modul2/animal/";
	char path_dir_darat[100] = "/home/lintang/modul2/darat/";
	char path_dir_air[100] = "/home/lintang/modul2/air/";


	//membuat folder modul2
	make_folder(path_dir_modul2);

	//membuat folder darat
	make_folder(path_dir_darat);

	//membuat folder air dg jeda 3 dtk
	sleep(3);
	make_folder(path_dir_air);

	//download animal.zip from gdrive
	download_animal_zipfile();

	//unzip animal.zip
	unzip_animal();


	dp = opendir(path_dir_animal);
      	if (dp != NULL)
	{
		while ((ep = readdir (dp)) != NULL) {
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
				char filename[100];
				strcpy(filename, path_dir_animal);
				strcat(filename, ep->d_name);
				if (strstr(ep->d_name, "darat") != NULL) {
					folder_darat(filename, path_dir_darat);
				}
				if (strstr(ep->d_name, "air") != NULL) {
					folder_air(filename, path_dir_air);
				}
				if (!(strstr(ep->d_name, "darat")) && !(strstr(ep->d_name, "air"))) {
					delete_hewan(filename);
				}
                       	}
                }
               	(void) closedir (dp);
	}

	//sleep(2); -> testing n debug
	dp = opendir(path_dir_darat);
	if (dp != NULL)
        {
		while ((ep = readdir (dp)) != NULL) {
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
	                        char filename[100];
				strcpy(filename, path_dir_darat);
	                        strcat(filename, ep->d_name);
	                        if (strstr(ep->d_name, "bird") != NULL) {
	                        	delete_hewan(filename);
				}
			}
                }
		(void) closedir (dp);
        }

	//membuat file list.txt
	list_file();

	//membuat list di list.txt
	list_hewan_dir_air(path_dir_air);


	return 0;
}
