#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include<json-c/json.h>
#include<dirent.h>
#include <wait.h>

void waitFor (unsigned int secs) {
    unsigned int retTime = time(0) + secs;   // Get finishing time.
    while (time(0) < retTime);               // Loop until it arrives.
}

void makeFile (int i, char* out ){

    int status;

    time_t now;
    struct tm time_struct;

    char time_buff[80] ;
    char file_time_name[100]; 
    char num_str[13];

    now = time(0);
    time_struct  = *localtime(&now) ;
    strftime(time_buff, sizeof(time_buff), "%H:%M:%S", &time_struct) ;

    
    sprintf(num_str, "%d", i);
    strcpy(file_time_name, time_buff);
    strcat(file_time_name, "_gacha_");
    strcat(file_time_name, num_str);
    strcat(file_time_name, ".txt");

    strcpy(out, file_time_name);
    waitFor(1);
}

char* fileList(char* dirname){
	DIR* dir = opendir(dirname);
	if(dir == NULL){
		return "zero";
	}
	struct dirent* entity;
	entity = readdir(dir);

	int randNum = 0;
	if(strcmp(dirname, "../../characters") == 0){
		randNum = (rand() % (48 - 1 + 1)) + 1;
        
	}else{
		randNum = (rand() % (130 - 1 + 1)) + 1;
	}
	
	int count = 0;
	char* jsonFile = "";
	while (entity != NULL){
		if(strcmp(entity->d_name, ".") != 0 && strcmp(entity->d_name, "..") != 0){
			count++;
			if(count == randNum ){
				jsonFile = entity->d_name;
				entity = readdir(dir);
			}
		}
		entity = readdir(dir);
	}
	return jsonFile;	
}

const char* jsonValue(char* path, char *key){
  FILE *fp;
	char buffer[3024];
	struct json_object *parsed_json;
	struct json_object *name;
	size_t i;	

    fp = fopen(path,"r");
    fread(buffer, 3024, 1,fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);

    json_object_object_get_ex(parsed_json, key, &name);
    const char*  value = json_object_get_string(name);
    return value;
}

char *folderName[] = {"gacha_gacha"};
char *fileName[] = {"character.zip", "weapon.zip"}; 

void downloadExtract(){
    char *url[] = {
        "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
        "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
    };


    pid_t childId ;
    int status;

    for(int i = 0; i < 2; i++){
        if((childId = fork()) == 0){
            execlp("wget", "wget", "--no-check-certificate", url[i], "-O", fileName[i], NULL);
        }
        while(wait(&status) > 0);
        if((childId = fork()) == 0){
            execlp("unzip", "unzip","-qq",fileName[i], NULL);
        }
        while(wait(&status) > 0);
    }
}

void timeToGacha(){
  pid_t childId;
  int status;

  if((childId = fork()) == 0){
      execlp("mkdir", "mkdir", "-p", "gacha_gacha", NULL);
  }
  while(wait(&status) > 0);
  if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha")) < 0) {
    exit(EXIT_FAILURE);
  }

  char fileName[40];
  char jsonPath[50];
  char num_str[13];
  char outFolder[101];
  char output[101];

  int primogems = 39000;
  int i = 0;
  char outFile[100];
  char tempFile[50];
  if((childId = fork()) == 0){
      execlp("mkdir", "mkdir", "-p","temp", NULL);
  }
  while(wait(&status) > 0);

  if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha/temp")) < 0) {
      exit(EXIT_FAILURE);
  }
  if((childId = fork()) == 0){
      execlp("touch", "touch", "temp.txt", NULL);
  }
  while(wait(&status) > 0);

  while (primogems > 159){
    i =i+1;
    primogems -= 160;
    if(i % 2 != 0){
      strcpy(jsonPath,"../../characters/");
      strcpy(fileName, fileList("../../characters"));
      strcat(jsonPath, fileName);

      const char * characterName = jsonValue(jsonPath, "name");
      const char * characterRarity = jsonValue(jsonPath, "rarity");
      
      sprintf(num_str, "%d", i);
      strcpy(output, num_str);
      strcat(output, "_characters_");
      strcat(output, characterRarity);
      strcat(output, "_");
      strcat(output, characterName);
      strcat(output, "_");
      sprintf(num_str, "%d", primogems);
      strcat(output, num_str);
      strcat(output, "\n");

      FILE *out=fopen("temp.txt","a");
      fputs(output,out);
      fclose(out);
    }else{
      strcpy(jsonPath,"../../weapons/");
      strcpy(fileName, fileList("../../weapons"));
      strcat(jsonPath, fileName);
      const char * weaponName = jsonValue(jsonPath, "name");

      const char * weaponRarity = jsonValue(jsonPath, "rarity");
      
      sprintf(num_str, "%d", i);
      strcpy(output, num_str);
      strcat(output, "_weapons_");
      strcat(output, weaponName);
      strcat(output, "_");
      strcat(output, weaponRarity);
      strcat(output, "_");
      sprintf(num_str, "%d", primogems);
      strcat(output, num_str);
      strcat(output, "\n");

      FILE *out=fopen("temp.txt","a");
      fputs(output,out);
      fclose(out);
    }
    if(i % 10 == 0){
      makeFile(i, outFile);
      if((childId = fork()) == 0){
          execlp("mv", "mv", "temp.txt", outFile, NULL);
      }
      while(wait(&status) > 0);
      if(i % 90 != 0){
        if((childId = fork()) == 0){
            execlp("touch", "touch", "temp.txt", NULL);
        }
        while(wait(&status) > 0);
      }
    }
    if(i % 90 == 0){
      if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha")) < 0) {
          exit(EXIT_FAILURE);
      }
      sprintf(num_str, "%d", i);
      strcpy(outFolder, "jumlah_gacha_");
      strcat(outFolder, num_str);
      
      if((childId = fork()) == 0){
          execlp("mv", "mv", "temp", outFolder, NULL);
      }
      while(wait(&status) > 0);

      if((childId = fork()) == 0){
          execlp("mkdir", "mkdir", "temp", NULL);
      }
      while(wait(&status) > 0);

      if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha/temp")) < 0) {
          exit(EXIT_FAILURE);
      }

    }
  }

//ubah nama file dan folder temp yang belum mencapai modulo 10 pada file dan modulo 90 pad folder
  if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha/temp")) < 0) {
      exit(EXIT_FAILURE);
  }
  makeFile(i, outFile);
  if((childId = fork()) == 0){
      execlp("mv", "mv", "temp.txt", outFile, NULL);
  }
  while(wait(&status) > 0);
  
  if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha")) < 0) {
      exit(EXIT_FAILURE);
  }
  sprintf(num_str, "%d", i);
  strcpy(outFolder, "jumlah_gacha_");
  strcat(outFolder, num_str);
  
  if((childId = fork()) == 0){
      execlp("mv", "mv", "temp", outFolder, NULL);
  }
  while(wait(&status) > 0);
}


// kelompok D04
int main() {
  pid_t pid, sid;

  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  downloadExtract();

  if ((chdir("/home/yunus/Praktikum/modul2")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);


  while (1) {
    pid_t childId;
    int status;

    time_t now = time(NULL);
    struct tm * currTime = localtime(&now);
  

    if (
      currTime->tm_mday == 30 &&
      currTime->tm_mon  == 2  &&
      currTime->tm_hour == 4 &&
      currTime->tm_min  == 44 &&
      currTime->tm_sec == 0 

    ) {
      timeToGacha();
    } else if (
      currTime->tm_mday == 30  &&
      currTime->tm_mon  == 2  &&
      currTime->tm_hour == 7 &&
      currTime->tm_min  == 44 &&
      currTime->tm_sec == 0 
    ) {
      if ((chdir("/home/yunus/Praktikum/modul2")) < 0) {
          exit(EXIT_FAILURE);
      }
      if((childId = fork()) == 0) {
        execlp("zip", "zip", "-P", "satuduatiga", "-rm","not_safe_for_wibu.zip", "gacha_gacha" , NULL);
      }
      while(wait(&status) > 0);
    }

    while(wait(&status) > 0);

    sleep(1);
  }
}
