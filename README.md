# Soal Shift Sistem Operasi Modul 2 Kelompok D04 2022

## Anggota Kelompok

<table>
    <tr>
        <th>NRP</th>
        <th>Nama</th>
    </tr>
    <tr>
        <td>Muhammad Lintang Panjerino</td>
        <td>5025201045</td>
    </tr>
    <tr>
        <td>Muhammad Yunus</td>
        <td>5025201171</td>
    </tr>
    <tr>
        <td>PEDRO T KORWA</td>
        <td>05111940007003</td>
    </tr>
<table>


# Lapres Modul 2
## 1. Soal 1
#### Deskripsi Soal:
Mas Refadi adalah seorang wibu gemink. Dan jelas game favoritnya adalah bengshin impek.
Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu
ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena
rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program
untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih
suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan
program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi
keinginan nya itu.
untuk melaksanakan tugas-tugas berikut:
 Note:
- Menggunakan fork dan exec.
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename().
- Tidak boleh pake cron.
- Semua poin dijalankan oleh 1 script di latar belakang. Cukup jalankan script 1x serta ubah
time dan date untuk check hasilnya.
- Link
Database item characters :
https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view
Database item weapons :
https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view

Tips :
- Gacha adalah proses untuk mendapatkan suatu item dengan cara melakukan randomize dari
seluruh item yang ada.
- DIkarenakan file database memiliki format (.json). Silahkan gunakan library <json-c/json.h>,
install dengan “apt install libjson-c-dev”, dan compile dengan “gcc [nama_file] -l json-c -o
[nama_file_output]”. Silahkan gunakan dokumentasi berikut untuk membaca dan parsing
file (.json).
https://progur.com/2018/12/how-to-parse-json-in-c.html

### 1.A
Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).
#### Pembahasan
Download semua folder yang dibutuhkan dengan mengunakan `wget`. Sebelumnya link dowload harus diubah terlebih dahulu yang awalnya merupakan link untuk view, diubah menjadi dowload seperti berikut:
``` bash
char *url[] = {
"https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
"https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
};
```
Lalu jalankan fork untuk mendowload kedua folder tersebut dengan `wget`, setelah didownload lanjutkan proses untuk melakukan unzip pada folder hasil download dengan `unzip` seperti dengan nama file:
``
char *fileName[] = {"character.zip", "weapon.zip"};
``
lalu jalankan seperti berikut:
```c
pid_t  childId ;
int  status;

for(int  i = 0; i < 2; i++){
	if((childId = fork()) == 0){
	execlp("wget", "wget", "--no-check-certificate", url[i], "-O", fileName[i], NULL);
}
while(wait(&status) > 0);
if((childId = fork()) == 0){
	execlp("unzip", "unzip","-qq",fileName[i], NULL);
}
while(wait(&status) > 0);
```
Program diatas akan diletakkan pada satu fungsi yaitu `downloadExtract`

Selanjutnya --
Buat folder `gacha_gacha` dengan mengunakan `fork`  lalu jadikan folder tersebut sebagai *working directory* dengan mengunakan `chdir`
``` c
pid_t  childId;
int  status;

if((childId = fork()) == 0){
execlp("mkdir", "mkdir", "-p", "gacha_gacha", NULL);
}
while(wait(&status) > 0);
if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha")) < 0) {
exit(EXIT_FAILURE);
}
```

### 1.B

Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai 

1. 
- genap akan dilakukan gacha item weapons, 
- jika bernilai ganjil maka item
characters. 

2. 
- Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat
sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. 

3. 
- Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah
folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.
Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. 

4. 
- Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA

#### Pembahasan
-- 1
Kita hanya perlu melakukan pengkondisian jika iterasi atau gacha saat itu merupakan (i) mod 2 != 0 maka baca folder path dari folder character sebaliknya baca dari folder weapon.

-- 2 & 3
Pertama-tama saya mulai dengan membuat suatu folder `temp` lalu kita berpindah ke folder temp dengan `chdir`. Saat sudah berada dalam folder temp, suatu file `temp.txt` akan dibuat. Dari sinilah nantinya gacha akan dilakukan dan hasil gacha akan diltakkan pada file `temp.txt` tersebut. setiap kali gacha dapat di mod 10, maka file `temp.txt` akan direname sesuai nama folder yang diinginkan pada nomor 1.c nanti, setelah di rename folder temp.txt lainnya akan kembali dibuat untuk diisi dengan output kecuali pada gacha mod 90 karena kita akan membuat `temp.txt` pada folder baru.
`i = gacha saat itu` 
```c
if(i % 10 == 0){
//make file menghasilkan nama file yang inginkan (soal 1.c)
	makeFile(i, outFile);
	if((childId = fork()) == 0){
		execlp("mv", "mv", "temp.txt", outFile, NULL);
	}
	while(wait(&status) > 0);
	if(i % 90 != 0){
		if((childId = fork()) == 0){
			execlp("touch", "touch", "temp.txt", NULL);
		}
	while(wait(&status) > 0);
	}
}
```
Selanjutnya pada saat gacha telah dapat dimodulo 90, maka kita akan keluar dari folde `temp` yang sedang dipakai pada saat itu, lalu mengubah nama folder `temp` dengan nama folder yang diinginkan pada soal 1.c, lalubuat folder `temp` lainnya dan masuk kembali ke folder temp tersebut dan kembali buat file `temp.txt` untuk kembali dilakukan gacha. 
```c
if(i % 90 == 0){
	if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha")) < 0) {
		exit(EXIT_FAILURE);
	}
	sprintf(num_str, "%d", i);
	strcpy(outFolder, "jumlah_gacha_");
	strcat(outFolder, num_str);
	if((childId = fork()) == 0){
		execlp("mv", "mv", "temp", outFolder, NULL);
	}
	while(wait(&status) > 0);
	if((childId = fork()) == 0){
		execlp("mkdir", "mkdir", "temp", NULL);
	}
	while(wait(&status) > 0);
	if ((chdir("/home/yunus/Praktikum/modul2/gacha_gacha/temp")) < 0) {
		exit(EXIT_FAILURE);
	}
}
```
Siklus ini akan diulangi terus menerus hingga gacha selesai. 

-- 4
Untuk dapat melakukan gacha, mengambil file json dari  setiap character dan weapon dari folder yang sudah diextract/unzip secara random disini saya memanfaat kan fungsi rand() untuk menghasilkan angka random dari 1 hingga 48 untuk folder character, karena ada maksimal 48 character dalam folder. 1 hingga 130 untuk folder weapon  karena alasan yang sama.
Sebelumnya folder tersebut harus dibuka dan dibaca terlebih dahulu dengan mengunakan `opendir(<dirname>)`  lalu membaca directory tersebut dengan `readdir(<dirVar>)` lalu simpan pada sebuah pointer struct dirent.
Setelah berhail dibaca tinggal lakukan looping untuk membaca semua isi yang ada difolder tersebut dan siapkan variable count, karena kita akan mengambil folder secara random yaitu saat `count == rand() `yang telah kita buat pada fungsi berikut:
```c
DIR* dir = opendir(dirname);
if(dir == NULL){
	return  "zero";
}
struct  dirent* entity;
entity = readdir(dir);

if(strcmp(dirname, "../../characters") == 0){
//untuk folder char
	randNum = (rand() % (48 - 1 + 1)) + 1;
}else{
//untuk folder weapon
	randNum = (rand() % (130 - 1 + 1)) + 1;
}
```
Jadi saat `count == rand()` baru file json diambil pathnya lalu dikembalikan untuk dibaca filenya. 
```c
int  count = 0;
char* jsonFile = "";

while (entity != NULL){
	if(strcmp(entity->d_name, ".") != 0 && strcmp(entity->d_name, "..") != 0){
count++;
		if(count == randNum ){
			jsonFile = entity->d_name;
			entity = readdir(dir);
			}
	}
	entity = readdir(dir);
}
return jsonFile;
```
Dengan begitu kita akan selalu menadapatkan random jsonFile untuk dibaca.

### 1.C
Format penamaan setiap 
1. file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha},
misal 04:44:12_gacha_120.txt, dan 
2.  format penamaan untuk setiap folder nya adalah
total_gacha_{jumlah-gacha}, misal total_gacha_270. 
3.  Dan untuk setiap file (.txt) akan
memiliki perbedaan penamaan waktu output sebesar 1 second.

#### Pembahasan
-- 1
Dapatkan waktu gacha dengan mengunakan library time 
```c
time_t  now;
struct  tm  time_struct;
char  time_buff[80] ;
char  file_time_name[100];
char  num_str[13];

now = time(0);
time_struct = *localtime(&now) ;
```
lalu outputkan hasil agar sesuai format dengan `strftime()` 
```c
strftime(time_buff, sizeof(time_buff), "%H:%M:%S", &time_struct) ;
```
`time_buff` akan menjadi string dengan format penamaan tanggal yang telah sesuai.
Buat suatu variable untuk menyimpan string format nama lalu constrcut/bentuk format nama yang diinginkan dengan memanfaatkan string cpy, cat 
`i == gacha saat ini`
```c
sprintf(num_str, "%d", i);
strcpy(file_time_name, time_buff);
strcat(file_time_name, "_gacha_");
strcat(file_time_name, num_str);
strcat(file_time_name, ".txt");
strcpy(out, file_time_name);
waitFor(1);
``` 
Dengan begitu pada saat ini pointer file_time_name telah memiliki format panamaan yang sesui dan siap untuk dan dijadikan format penamaan file.

-- 2
Untuk format penamaan folder cukup dengan mgneunakan `total_gacha_{i}`
dengan `i == gacha saat ini`

-- 3 
Untuk melakukan delay 1 detik dalam pengambilan gacha, maka dalam penamaan file nanti juga akan ter-delay 1 detik oleh karena itu dalam penamaan file saya memanggil fungsi `waitFor(1)` yang merupakan fungsi untuk menghentikan prorgam selama satu detik sebelum dilanjutkan kembali
```c
void  waitFor (unsigned  int  secs){
	unsigned  int  retTime = time(0) + secs; // Get finishing time.
	while (time(0) < retTime); // Loop until it arrives.
}
```

### 1.D
Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. 
1.  Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. 
2. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. 
3. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}.

- Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880

#### Pembahasan
-- 1
Karena progrma akan terus berjalan hingga primogems habis dan harga 1 kali gacha adalah 160, maka tetapkan terlebih dahulu julah primogems selanjutnya lakukan looping untuk melakukan gacha selama jumlah primogems masih >159. Dan tetapkan variable 1 untuk menghitung gacha ke berapa saat itu, dan setiap looping primogems akan terus dikurang 160		
```c
int primogems = 79000;
while (primogems > 159){
	i =i+1;
	primogems -= 160;
	...
}
```
-- 2
Untuk membaca file json maka kita akan memanfaat library json-c sesui dengan tips pada soal ini. Pertama kita harus menentukan folder apa yang akan dibaca terlebih dahulu, sesuai dengan petunjuk pada soal 1.b jika i ganjil ambil character, jika genap ambil weapon.
```c
char  jsonPath[50]; //json path untuk menyimpan path folder yang akan dibaca
if(i % 2 != 0){
	strcpy(jsonPath,"../../characters/");
	strcpy(fileName, fileList("../../characters")); 
	//file list adalah fungsi yang sudah dijelaskan pada soal 1.b yaitu fungsi untuk mendapatkan path dari folder json file yang akan dibaca
	strcat(jsonPath, fileName);
	...
else{
	strcpy(jsonPath,"../../weapons/");
	strcpy(fileName, fileList("../../weapons"));
	strcat(jsonPath, fileName);
	...
}
```
Selanjutnya jalankan fungsi jsonValue untuk mendaaptkan properti name dan rarity dari file json yang akan dibaca.
jsonValue akan menerima 2 argumen yaitu filePath dari json file yang sudah dipilih dan akan dibaca, dan key atau properti yang akan dibaca dari file json tersebut.
Pada fungsi ini lah library json-c digunakan. Pertama-tama buka file dengan fopen dengan metoh read lalu hasil yang telah dibaca dari file akan disimpan pada buffer, dan file ditutup.
selanjutnya parse buffer yang berbentuk json tersebut dengan fungsi `json_tokener_parse(buffer)` dari library, selanjutnya dapatkan value dari key yang diingin dan simpan pada suatu variabel (dalam hal ini "name") dengan `json_object_object_get_ex(parsed_json, key, &name)` .
Terakhir dapatkan bentuk dari string dari value dari property yang didapatkan sebelumnua denga fungsi `json_object_get_string(name)` dan return hasilnya,
Dengan fungsi ini kita sudah berhasil mendaptkan value dari property (name dan rarity) yang diinginak pada file json yang dipilih
```c
const  char * weaponName = jsonValue(jsonPath, "name");

const  char * weaponRarity = jsonValue(jsonPath, "rarity");
```
```c
const  char* jsonValue(char* path, char *key){
	FILE *fp;
	char  buffer[3024];
	struct  json_object *parsed_json;
	struct  json_object *name;
	size_t  i;
	
	fp = fopen(path,"r");
	fread(buffer, 3024, 1,fp);
	fclose(fp);
	parsed_json = json_tokener_parse(buffer);
	
	json_object_object_get_ex(parsed_json, key, &name);
	const  char* value = json_object_get_string(name);
	return  value;
}
```
-- 3
Dari value properti name dan rarity yang sudah didapatkan lalu constract string yang akan di outputkan pada file gacha yaitu `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}`
```c
sprintf(num_str, "%d", i);
strcpy(output, num_str);
strcat(output, "_characters_");
strcat(output, characterRarity);
strcat(output, "_");
strcat(output, characterName);
strcat(output, "_");
sprintf(num_str, "%d", primogems);
strcat(output, num_str);
strcat(output, "\n");
```
lalu append ke file gacha yang sudah dibuat dalam hal ini pada file temp.txt terlebih dahulu sebelum nantinya temp.txt akan direname sesuai solusi pada soal 1.B
```c
FILE *out=fopen("temp.txt","a");
fputs(output,out);
fclose(out);
```


### 1.E
1. Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary
pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44. 
2. Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama
not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di
delete sehingga hanya menyisakan file (.zip)
#### Pembahasan
--1 
Manfaat kan library time.h dengan mengunan time dari system dan jalankan program saat waktu telah sesuia yang diinginkan
```c
ime_t  now = time(NULL);
struct  tm * currTime = localtime(&now);

if (
currTime->tm_mday == 30 &&
currTime->tm_mon == 2 &&
currTime->tm_hour == 4 &&
currTime->tm_min == 44 &&
currTime->tm_sec == 0
) {
	timeToGacha();
}
```
-- 2
Dengan cara yang sama, jalankan program untuk zip folder gacha-gacha sekaligus menghapus foldernya dan memeberikan password pada file dalam zip tersebut pada waktu yang dinginkan (3 jam setelah = 7:44) namun sebelumnya pastikan kita telah keluar dari working directory gacha_gacha karena kita akan melakukan proses zip dan delete pada folder tersebut dari paren foldernya.
seperti berikut:
```c
else  if (
	currTime->tm_mday == 30 &&
	currTime->tm_mon == 2 &&
	currTime->tm_hour == 7 &&
	currTime->tm_min == 44 &&
	currTime->tm_sec == 0
){
	if ((chdir("/home/yunus/Praktikum/modul2")) < 0) {
		exit(EXIT_FAILURE);
	}
	if((childId = fork()) == 0) {
		execlp("zip", "zip", "-P", "satuduatiga", "-rm","not_safe_for_wibu.zip", "gacha_gacha" , NULL);
	}
	while(wait(&status) > 0);
}
```
### NOTE
Program ini akan dijalankan sekali di latar belakang, oleh karena iin program ini merupakan program daemon maka kita harus menjalankan program dalam bentuk daemon. Dengan code seperti berikut sesuia template daemon pada modul

```c
int  main() {
	pid_t  pid, sid;
	pid = fork();

	if (pid < 0) {
		exit(EXIT_FAILURE);
	}
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}
	umask(0);
	sid = setsid();
	if (sid < 0) {
		exit(EXIT_FAILURE);
	}
	//download dan exract file dapat dilakukan lebih dulu dan hanyta sekali 
	downloadExtract();
	//working directory awal
	if ((chdir("/home/yunus/Praktikum/modul2")) < 0) {
		exit(EXIT_FAILURE);
	}
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	while (1) {
		//... code program
		sleep(1);
	}
}
```

<h3> Berikut adalah video bukti jalannya program SOAL1 </h3>
> https://drive.google.com/file/d/1-wqWegGo8IPJhZRuOKLGTU6s3bUl3goL/view?usp=sharing


### Kendala
- Cukup kesulitan melakukan testing saat mengunakan daemon.
-  Bingun dalam pengunaan fungsi untuk membaca, menambahkan folder/file dari c
- belum terbiasa dalam mengunakan fork
- Sempat salah dalam menentukan waktu running karena bulan dalam library time.h dimulai dari 0 - 11, bukan 1- 12

# 2. Soal 2

## Deskripsi Soal

Pada soal 2 diberikan perintah untuk me-*review*/mengulas drama korea yang tayang dan sedang ramai di Layanan Streaming Film.

Untuk mendapatkan data drama korea yang berupa poster-poster drama korea, hal yang harus dilakukan pertama kali adalah membuat folder/directory di **/home/[user]/** dengan nama "shift2" yang di dalamnya ada folder bernama "drakor". Setelah itu, program harus download file **drakor.zip** dari google drive lalu meng-*extract*-nya ke dalam folder **/home/[user]/shift2/drakor**. Namun, hanya file yang penting yang akan digunakan, yaitu file dengan format **.png**.

Setelah poster-poster drama korea di-extract, akan dibuat beberapa folder untuk setiap jenis drama korea yang ada dalam zip dengan nama folder yang sama dengan nama jenisnya. Karena jumlah poster yang banyak dan tidak memungkinkan untuk mengecek manual, program harus membuat folder-folder yang dibutuhkan, misalnya folder **/drakor/romance** atau **/drakor/school**. Format penamaan file pada awalnya adalah **[nama]:[tahun rilis]:[kategori]**.

Setelah semua folder kategori dibuat, poster-poster drama korea akan dipindahkan ke folder sesuai dengan kategorinya dan nama filenya diubah menjadi nama dramanya, misal **/drakor/romance/start-up.png**.

Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Misal: foto dengan nama **start-up;2020;romance_the-k2;2016;action.png** dipindah ke 2 folder, yaitu **/drakor/romance/start-up.png** dan **/drakor/action/the-k2.png**. Foto yang terdapat lebih dari satu poster/drama ditandai dengan adanya underscore(_) sebagai pemisah kedua drama.

Yang terakhir, pada setiap folder kategori, program diminta untuk membuat file **data.txt** yang berisi nama kategori, nama drama korea, dan tahun rilis. List drama akan diurutkan secara *ascending* menurut tahun rilis masing-masing drama.

## Pembahasan dan Cara Pengerjaan

### 2a)

Membuat folder shift2 dan drakor dengan path folder **/home/[user]/shift2** dan **/home/[user]/shift2/drakor**. Setelah itu mendownload data drama korea dari google drive yaitu file drakor.zip kemudian di-unzip di folder **/home/[user]/shift2/drakor**. File yang di-unzip adalah file dengan format **.png**. Oleh karena itu, dibuat 3 fungsi, yaitu make_folder_drakor(), download_zip_drakor(), dan unzip_drakor().

Berikut adalah fungsi make_folder_drakor(), download_zip_drakor(), dan unzip_drakor():

```c
void make_folder_drakor(char *folder_path){

	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"mkdir", "-p", folder_path, NULL};
		execv("/bin/mkdir", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

//download from : https://drive.google.com/file/d/1Tf1bOHDd5IUnUZUjTIYDJhyDCsy3Vvis/view?usp=sharing
void download_zip_drakor(){
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char url[250] = "https://docs.google.com/uc?export=download&id=1Tf1bOHDd5IUnUZUjTIYDJhyDCsy3Vvis";
                char *argv[] = {"wget", "--no-check-certificate", url, "-O", "/home/lintang/shift2/drakor/drakor.zip", NULL};
                execv("/usr/bin/wget", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

void unzip_drakor(){
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"unzip", "-n", "/home/lintang/shift2/drakor/drakor.zip", "*.png", "-d", "/home/lintang/shift2/drakor/", NULL};
                execv("/usr/bin/unzip", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}
```

Berikut adalah pemanggilan fungsi make_folder_drakor(), download_zip_drakor(), dan unzip_drakor() :

```c
	char user[20];
	strcpy(user, getenv("USER"));

	if ((chdir("/")) < 0) {
		exit(EXIT_FAILURE);
	}
	if ((chdir("home")) < 0) {
		exit(EXIT_FAILURE);
	}
	if ((chdir(user)) < 0) {
		exit(EXIT_FAILURE);
	}

        make_folder_drakor("shift2/drakor");

        if ((chdir("shift2/drakor")) < 0) exit(EXIT_FAILURE);

	download_zip_drakor();

	unzip_drakor();
```

### 2b)

Membuat folder-folder sesuai dengan kategori tiap drama dengan menggunakan fungsi **make_kategori()** dan dengan bantuan fungsi **nama_kategori()**. Caranya adalah dengan menggunakan directory listing pada fungsi make_kategori() untuk men-*traverse* tiap file pada directory tertentu. Nama file yang sedang di-*traverse* akan dikirim ke fungsi nama_kategori() untuk dijadikan argumen fungsi tersebut. Pada fungsi nama_kategori() akan digunakan fungsi strtok() untuk memecah nama file menjadi beberapa bagian, sehingga pada bagian ke-3 atau ke-6 akan didapatkan nama kategorinya. Setelah mendapatkan nama kategori, dijalankan **wait x fork x exec** untuk menjalankan perintah mkdir untuk membuat folder sesuai dengan nama kategorinya.

Berikut adalah fungsi make_kategori() dan nama_kategori() :

```c
void nama_kategori(char* filename){
	char* token;
	int i = 0;

	token = strtok(filename, "_;.");

	while( token != NULL ) {
		i++;
	        if(i == 3 || i == 6){
		        pid_t child_id;
		        int status;
		        child_id = fork();
		        if (child_id < 0) {
				exit(EXIT_FAILURE);
			}

		        if (child_id == 0) {
				char *argv[] = {"mkdir", "-p", token, NULL};
				execv("/usr/bin/mkdir", argv);
		        } else {
				while ((wait(&status)) > 0);
			}
	        }
	        token = strtok(NULL, "_;.");
	}
}

void make_kategori(){
	DIR *dp;
	struct dirent *ep;

	dp = opendir(".");

	if (dp != NULL)	{
		while ((ep = readdir (dp))) {
			if(strstr(ep->d_name, ".png"))
				nama_kategori(ep->d_name);
		}
		(void) closedir (dp);
	} else perror ("Couldn't open the directory");
}
```

Sedangkan berikut adalah pemanggilan fungsi make_kategori() dan nama_kategori() di main:

```c
	make_kategori();

	move_file();
```

### 2c) dan 2d)

Memindahkan semua file poster drama korea ke folder sesuai dengan kategorinya dengan menggunakan fungsi move_file(). Pada fungsi move_file() dijalankan directory listing untuk men-*traverse* semua file di folder **/home/[user]/shift2/drakor**, dan dengan bantuan fungsi strstr() hanya file-file dengan format **.png** yang akan dipindah ke masing-masing folder kategori.

Berikut adalah fungsi move_file() :

```c
void move_file(){
	DIR *dp;
	struct dirent *ep;
	FILE *fptr1, *fptr2;

	dp = opendir(".");

	if (dp != NULL)
	{
		while ((ep = readdir (dp))) {
			if(strstr(ep->d_name, ".png")) {
				int counter = 0;
				char* token, folder_name[255], poster_name_1[50], poster_name_2[50], poster_name_3[50], temp, tempfile[100];

				strcpy(tempfile, ep->d_name);

				token = strtok(tempfile, "_;.");

				while( token != NULL ) {
					counter++;
					if ((counter == 1 || counter == 4) && strcmp(token, "png") != 0){
						strcpy(poster_name_1, token);
					}
					else if (counter == 2 || counter == 5){
						strcpy(poster_name_2, token);
					}
					else if (counter == 3 || counter == 6){
						strcpy(poster_name_3, token);
						sprintf(folder_name, "%s/%s_%s_%s.png", poster_name_3, poster_name_2, poster_name_1, poster_name_3);

						fptr1 = fopen(ep->d_name, "rb");
						fptr2 = fopen(folder_name, "wb");

						while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);

						fclose(fptr1);
						fclose(fptr2);
					}

					token = strtok(NULL, "_;.");
				}
			}
		}

		closedir (dp);
	}
}

```

Sedangkan berikut adalah pemanggilan fungsi move_file() di main :

```c
	move_file();
```

### 2e)

Perintah terakhir soal adalah membuat file "**data.txt**" di setiap folder kategori drakor. File tersebut berisi nama kategori, nama drama korea, dan tahun rilis dari setiap drakor. List drakor harus urut *ascending* menurut tahun rilisnya. Pada kasus ini, digunakan fungsi **list_data_drama()** yang menerima 1 parameter berupa sebuah nama directory/folder kategori untuk membuat file **data.txt** dan membuat list drakor di dalam file tersebut.

Berikut adalah fungsi list_data_drama() :

```c
void list_data_drama(char *dir){
	DIR *dp;
	struct dirent *ep;
	pid_t child_id;
	int index, counter, status;
	char poster_name[15][150], *token, name[50], year[50], folder_name[100], temp[255], temp2[255], temp3[255];
	FILE *fptr;

	dp = opendir(dir);

	index = 0;
	if (dp != NULL)
	{
		while ((ep = readdir (dp))) {
			if(strstr(ep->d_name, ".png") != NULL){
				strcpy(poster_name[index++], ep->d_name);
			}
		}
		closedir (dp);
	}

	//sorting tahun rilis
	for(int i = 0; i < index-1; i++) {
		for (int j=0; j < index-i-1; j++) {
			if(strcmp(poster_name[j], poster_name[j+1]) > 0){
				strcpy(temp, poster_name[j]);
				strcpy(poster_name[j], poster_name[j+1]);
				strcpy(poster_name[j+1], temp);
			}
		}
	}

	sprintf(folder_name, "%s/data.txt", dir);
	fptr = fopen(folder_name, "w");
	fprintf(fptr, "kategori : %s\n", dir);

	for(int i = 0; i < index; i++) {
		strcpy(temp, poster_name[i]);
		counter = 0;
		token = strtok(temp, "_");

	        while(token != NULL){
			counter++;
			if(counter == 1) {
				strcpy(year, token);
			}
			else if (counter == 2) {
				strcpy(name, token);
			}
			token = strtok(NULL, "_");
		}

		fprintf(fptr, "\nnama : %s\n", name);
		fprintf(fptr, "rilis : %s\n", year);
		sprintf(temp3, "%s/%s.png", dir, name);
		sprintf(temp2, "%s/%s", dir, poster_name[i]);

		child_id = fork();
		if (child_id < 0) {
			exit(EXIT_FAILURE);
		}

		if (child_id == 0) {
			char *argv[] = {"mv", temp2, temp3, NULL};
			execv("/bin/mv", argv);
		} else {
			while ((wait(&status)) > 0);
		}
	}
	fclose(fptr);
}
```

Berikut adalah pemanggilan fungsi list_data_drama() :

```c
	list_data_drama("thriller");
	list_data_drama("horror");
	list_data_drama("action");
	list_data_drama("comedy");
	list_data_drama("romance");
	list_data_drama("school");
	list_data_drama("fantasy");
```

## Kendala

- Pada awal pengerjaan masih bingung dengan penggunaan fork, exec, dan wait yang banyak sehingga pada akhirnya membaginya menjadi beberapa fungsi kemudian memanggilnya di **main**
- Pada awalnya kurang paham dengan fungsi strtok()
- Tidak tahu bagaimana cara untuk mendapatkan nama kategori

<br><br>

# 3. Soal 3

## Deskripsi Soal

Pada soal ini diberikan perintah untuk mengklasifikasikan hewan-hewan di kebun binatang yang hilang.

Cara untuk mengklasifikasi hewan-hewan tersebut yaitu dengan membuat 2 directory di **/home/[USER]/modul2/** dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.

Setelah itu, untuk mendapatkan data semua hewan dilakukan download file animal.zip yang sudah tersedia di google drive, kemudian file animal.zip tersebut di-*extract* / di-*unzip* di **/home/[USER]/modul2/**.

Hasil extract tersebut diklassifikasikan sesuai dengan nama hewan pada filenya menjadi hewan darat dan hewan air dengan ketentuan sebagai berikut:
- Untuk hewan darat dimasukkan ke folder **/home/[USER]/modul2/darat**
- Untuk hewan air dimasukkan ke folder **/home/[USER]/modul2/air**.
- Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu.
- Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

Setelah hewan darat dan air diklasifikasikan, ada permintaan khusus yaitu semua hewan burung (terdapat kata "bird" pada nama file) yang ada di folder **/home/[USER]/modul2/darat** harus dihapus.

Permintaan terakhir dari soal adalah membuat file list.txt di dalam folder **/home/[USER]/modul2/air**. Pada file list.txt tersebut diisi dengan daftar nama semua hewan air yang ada di folder **/home/[USER]/modul2/air** dengan format **UID_[UID file permission]_Nama File.[jpg/png]** dimana UID adalah user dari file tersebut dan file permission adalah permission dari file tersebut.

## Pembahasan dan Cara Pengerjaan

### 3a)

Membuat folder **modul2**, **darat**, dan **air** dengan cara membuat *function* **make_folder(char \*folder_path)** bertipe void dengan 1 parameter yaitu string path dari foldernya. Cara kerja *function* tersebut adalah dengan membuat serangkaian proses **fork x exec x wait** di mana isi dari exec adalah perintah membuat folder dengan nama folder berupa string path dari folder. Folder **darat** akan dibuat terlebih dulu, 3 detik kemudian baru dibuat folder **air**.

Berikut adalah *function* make_folder() : 

``` c
void make_folder(char *folder_path) {

	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

	if (child_id == 0) {
                char *argv[] = {"mkdir", "-p", folder_path, NULL};
                execv("/usr/bin/mkdir", argv);

        } else {
                while ((wait(&status)) > 0);
	}

}
```

Berikut adalah pemanggilan fungsi make_folder() untuk membuat folder **modul2**, **darat**, dan **air** : 

```c
char path_dir_modul2[50] = "/home/lintang/modul2/";
char path_dir_darat[100] = "/home/lintang/modul2/darat/";
char path_dir_air[100] = "/home/lintang/modul2/air/";

//membuat folder modul2
make_folder(path_dir_modul2);

//membuat folder darat
make_folder(path_dir_darat);

//membuat folder air dg jeda 3 dtk
sleep(3);
make_folder(path_dir_air);

```

### 3b)

Download file **animal.zip** dari google drive menggunakan fungsi **download_animal_zipfile()**, kemudian di-*extract*/di-*unzip* di folder **/home/[USER]/modul2/** menggunakan fungsi **unzip_animal()**.

Berikut adalah fungsi download_animal_zipfile() dan unzip_animal() : 

``` c
//download from my google drive
//wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1hR0qFnGeMnAgEwNUYiNt4LKcf6TT2EGx' -O /home/lintang/modul2/animal.zip

void download_animal_zipfile() {
	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
		char link[250] = "https://docs.google.com/uc?export=download&id=1hR0qFnGeMnAgEwNUYiNt4LKcf6TT2EGx";
		char *argv[] = {"wget", "--no-check-certificate", link, "-O", "/home/lintang/modul2/animal.zip", NULL};
		execv("/usr/bin/wget", argv);
	} else {
		while ((wait(&status)) > 0);
	}
}

//unzip animal.zip
void unzip_animal() {
	pid_t child_id;
        int status;
	child_id = fork();

	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
		char *argv[] = {"unzip", "/home/lintang/modul2/animal.zip", "-d", "/home/lintang/modul2/", NULL};
		execv("/usr/bin/unzip", argv);
	} else {
		while ((wait(&status)) > 0);
	}
}
```

Berikut adalah pemanggilan fungsi download_animal_zipfile() dan unzip_animal() :

```c
//download animal.zip from gdrive
download_animal_zipfile();

//unzip animal.zip
unzip_animal();
```

### 3c)

Setelah extract/unzip file animal.zip, file hewan diklasifikasikan dan dipisah menjadi hewan darat dan air sesuai nama filenya. Jika tidak mengandung nama "darat" ataupun "air" maka file dihapus. Untuk mengelompokkan hewan darat menggunakan fungsi folder_darat(), untuk mengelompokkan hewan air menggunakan fungsi folder_air(), dan untuk menghapus file yang tidak ada keterangan darat atau air menggunakan fungsi delete_hewan().

Berikut adalah fungsi folder_darat(), folder_air(), dan delete_hewan():

``` c
//folder hewan darat
void folder_darat(char *filename, char *path_dir_darat) {
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
		char *argv[] = {"mv", filename, path_dir_darat, NULL};
		execv("/usr/bin/mv", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

//folder hewan air
void folder_air(char *filename, char *path_dir_air) {
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"mv", filename, path_dir_air, NULL};
                execv("/usr/bin/mv", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

//hapus file bukan darat/air
void delete_hewan(char *filename) {
        pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"rm", filename, NULL};
		execv("/usr/bin/rm", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

```

Berikut adalah pemanggilan fungsi folder_darat(), folder_air(), dan delete_hewan() dengan menggunakan directory listing :

``` c
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path_dir_animal);
      	if (dp != NULL)
	{
		while ((ep = readdir (dp)) != NULL) {
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
				char filename[100];
				strcpy(filename, path_dir_animal);
				strcat(filename, ep->d_name);
				if (strstr(ep->d_name, "darat") != NULL) {
					folder_darat(filename, path_dir_darat);
				}
				if (strstr(ep->d_name, "air") != NULL) {
					folder_air(filename, path_dir_air);
				}
				if (!(strstr(ep->d_name, "darat")) && !(strstr(ep->d_name, "air"))) {
					delete_hewan(filename);
				}
                       	}
                }
               	(void) closedir (dp);
	}

```

### 3d)

Setelah diklasifikasikan sesuai dengan habitatnya, ada permintaan tambahan yaitu menghapus semua hewann burung yang mengandung kata "bird" pada nama filenya. Untuk itu digunakan fungsi delete_hewan() seperti pada nomor 3c.

Berikut adalah fungsi delete_hewan() :

``` c
void delete_hewan(char *filename) {
        pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"rm", filename, NULL};
		execv("/usr/bin/rm", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

```

Berikut adalah pemanggilan fungsi delete_hewan() untuk menghapus hewan-hewan burung dengan menggunakan directory listing :

``` c
        DIR *dp;
	struct dirent *ep;

        dp = opendir(path_dir_darat);
	if (dp != NULL)
        {
		while ((ep = readdir (dp)) != NULL) {
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
	                        char filename[100];
				strcpy(filename, path_dir_darat);
	                        strcat(filename, ep->d_name);
	                        if (strstr(ep->d_name, "bird") != NULL) {
	                        	delete_hewan(filename);
				}
			}
                }
		(void) closedir (dp);
        }
```

### 3e)

Permintaan terakhir adalah membuat file list.txt di folder **/home/[USER]/modul2/air**. Di dalam file list.txt akan diisi dengan nama semua hewan air yang ada di folder **/home/[USER]/modul2/air** dengan format **UID_[UID file permission]_Nama File.[jpg/png]** dimana UID adalah user dari file tersebut dan file permission adalah permission dari file tersebut. Dalam kasus ini, dibuat 2 fungsi yaitu **list_file()** dan **list_hewan_dir_air()**. Fungsi **list_file()** menggunakan perintah touch untuk membuat file sedangkan fungsi **list_hewan_dir_air()** menggunaknan directory listing dan di dalamnya terdapat file ownership dan file permission untuk mendapat UID dan UID file permission.

Berikut adalah fungsi **list_file()** dan **list_hewan_dir_air()** :

``` c
void list_file() {
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"touch", "/home/lintang/modul2/air/list.txt", NULL};
                execv("/usr/bin/touch", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

void list_hewan_dir_air(char *basePath)
{
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if (!dir)
        	return;

	FILE *fptr;
	char fname[150];
	strcpy(fname, basePath);
	strcat(fname, "list.txt");

	fptr = fopen(fname, "a+");


	while ((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
		{
			char list_name[150] = "";

			//file ownership (user)
			struct stat info;
			char ownership_name[100] = "";
			int r1;

			char temp1[100] = "";
			strcpy(temp1, basePath);
                        strcat(temp1, dp->d_name);
			r1 = stat(temp1, &info);
			if( r1==-1 )
			{
				fprintf(stderr,"File error\n");
				exit(1);
			}

			struct passwd *pw = getpwuid(info.st_uid);
			struct group  *gr = getgrgid(info.st_gid);

			if (pw != 0) strcat(ownership_name, pw->pw_name);

			strcat(list_name, ownership_name);
			strcat(list_name, "_");

			//file permission (owner)
			struct stat fs;
			char permission_name[100] = "";
			int r2;

			char temp2[100] = "";
			strcpy(temp2, basePath);
			strcat(temp2, dp->d_name);
			r2 = stat(temp2,&fs);
			if( r2==-1 )
			{
				fprintf(stderr,"File error\n");
				exit(1);
			}

			if( fs.st_mode & S_IRUSR ) {
				strcat(permission_name, "r");
			}
			if( fs.st_mode & S_IWUSR ) {
				strcat(permission_name, "w");
			}
			if( fs.st_mode & S_IXUSR ) {
				strcat(permission_name, "x");
			}


			strcat(list_name, permission_name);
			strcat(list_name, "_");

			if (!(strstr(dp->d_name, "txt"))) {
				strcat(list_name, dp->d_name);
                                fprintf(fptr, "%s\n", list_name);
                        }
		}
	}

	closedir(dir);
	fclose(fptr);
}

```

Berikut adalah pemanggilan fungsi list_file() dan list_hewan_dir_air() :


``` c
        //membuat file list.txt
	list_file();

	//membuat list di list.txt
	list_hewan_dir_air(path_dir_air);
```

### Kendala

- Pada awal pengerjaan masih bingung dengan penggunaan fork, exec, dan wait yang banyak sehingga pada akhirnya membaginya menjadi beberapa fungsi kemudian memanggilnya di **main**
- Sempat bermasalah dengan fungsi list_hewan_dir_air(): ketika program dijalankan, file list.txt akan berisi nama-nama file hewan air yang berulang berkali-kali
