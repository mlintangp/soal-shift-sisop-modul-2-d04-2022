#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

void make_folder_drakor(char *folder_path){

	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"mkdir", "-p", folder_path, NULL};
		execv("/bin/mkdir", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

//download from : https://drive.google.com/file/d/1Tf1bOHDd5IUnUZUjTIYDJhyDCsy3Vvis/view?usp=sharing
void download_zip_drakor(){
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char url[250] = "https://docs.google.com/uc?export=download&id=1Tf1bOHDd5IUnUZUjTIYDJhyDCsy3Vvis";
                char *argv[] = {"wget", "--no-check-certificate", url, "-O", "/home/lintang/shift2/drakor/drakor.zip", NULL};
                execv("/usr/bin/wget", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

void unzip_drakor(){
	pid_t child_id;
        int status;
        child_id = fork();

        if (child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
                char *argv[] = {"unzip", "-n", "/home/lintang/shift2/drakor/drakor.zip", "*.png", "-d", "/home/lintang/shift2/drakor/", NULL};
                execv("/usr/bin/unzip", argv);
        } else {
                while ((wait(&status)) > 0);
        }
}

void nama_kategori(char* filename){
	char* token;
	int i = 0;

	token = strtok(filename, "_;.");

	while( token != NULL ) {
		i++;
	        if(i == 3 || i == 6){
		        pid_t child_id;
		        int status;
		        child_id = fork();
		        if (child_id < 0) {
				exit(EXIT_FAILURE);
			}

		        if (child_id == 0) {
				char *argv[] = {"mkdir", "-p", token, NULL};
				execv("/usr/bin/mkdir", argv);
		        } else {
				while ((wait(&status)) > 0);
			}
	        }
	        token = strtok(NULL, "_;.");
	}
}

void make_kategori(){
	DIR *dp;
	struct dirent *ep;

	dp = opendir(".");

	if (dp != NULL)	{
		while ((ep = readdir (dp))) {
			if(strstr(ep->d_name, ".png"))
				nama_kategori(ep->d_name);
		}
		(void) closedir (dp);
	} else perror ("Couldn't open the directory");
}

void move_file(){
	DIR *dp;
	struct dirent *ep;
	FILE *fptr1, *fptr2;

	dp = opendir(".");

	if (dp != NULL)
	{
		while ((ep = readdir (dp))) {
			if(strstr(ep->d_name, ".png")) {
				int counter = 0;
				char* token, folder_name[255], poster_name_1[50], poster_name_2[50], poster_name_3[50], temp, tempfile[100];

				strcpy(tempfile, ep->d_name);

				token = strtok(tempfile, "_;.");

				while( token != NULL ) {
					counter++;
					if ((counter == 1 || counter == 4) && strcmp(token, "png") != 0){
						strcpy(poster_name_1, token);
					}
					else if (counter == 2 || counter == 5){
						strcpy(poster_name_2, token);
					}
					else if (counter == 3 || counter == 6){
						strcpy(poster_name_3, token);
						sprintf(folder_name, "%s/%s_%s_%s.png", poster_name_3, poster_name_2, poster_name_1, poster_name_3);

						fptr1 = fopen(ep->d_name, "rb");
						fptr2 = fopen(folder_name, "wb");

						while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);

						fclose(fptr1);
						fclose(fptr2);
					}

					token = strtok(NULL, "_;.");
				}
			}
		}

		closedir (dp);
	}
}

void list_data_drama(char *dir){
	DIR *dp;
	struct dirent *ep;
	pid_t child_id;
	int index, counter, status;
	char poster_name[15][150], *token, name[50], year[50], folder_name[100], temp[255], temp2[255], temp3[255];
	FILE *fptr;

	dp = opendir(dir);

	index = 0;
	if (dp != NULL)
	{
		while ((ep = readdir (dp))) {
			if(strstr(ep->d_name, ".png") != NULL){
				strcpy(poster_name[index++], ep->d_name);
			}
		}
		closedir (dp);
	}

	//sorting tahun rilis
	for(int i = 0; i < index-1; i++) {
		for (int j=0; j < index-i-1; j++) {
			if(strcmp(poster_name[j], poster_name[j+1]) > 0){
				strcpy(temp, poster_name[j]);
				strcpy(poster_name[j], poster_name[j+1]);
				strcpy(poster_name[j+1], temp);
			}
		}
	}

	sprintf(folder_name, "%s/data.txt", dir);
	fptr = fopen(folder_name, "w");
	fprintf(fptr, "kategori : %s\n", dir);

	for(int i = 0; i < index; i++) {
		strcpy(temp, poster_name[i]);
		counter = 0;
		token = strtok(temp, "_");

	        while(token != NULL){
			counter++;
			if(counter == 1) {
				strcpy(year, token);
			}
			else if (counter == 2) {
				strcpy(name, token);
			}
			token = strtok(NULL, "_");
		}

		fprintf(fptr, "\nnama : %s\n", name);
		fprintf(fptr, "rilis : %s\n", year);
		sprintf(temp3, "%s/%s.png", dir, name);
		sprintf(temp2, "%s/%s", dir, poster_name[i]);

		child_id = fork();
		if (child_id < 0) {
			exit(EXIT_FAILURE);
		}

		if (child_id == 0) {
			char *argv[] = {"mv", temp2, temp3, NULL};
			execv("/bin/mv", argv);
		} else {
			while ((wait(&status)) > 0);
		}
	}
	fclose(fptr);
}

void remove_file(){
	DIR *dp;
	struct dirent *ep;
	dp = opendir(".");

	if (dp != NULL)
	{
		while ((ep = readdir (dp))) {
			if(strstr(ep->d_name, ".png")){
				remove(ep->d_name);
			}
		}
		closedir (dp);
	}
}

int main(){
	pid_t child_id;
	int status;
	char user[20];
	strcpy(user, getenv("USER"));

	if ((chdir("/")) < 0) {
		exit(EXIT_FAILURE);
	}
	if ((chdir("home")) < 0) {
		exit(EXIT_FAILURE);
	}
	if ((chdir(user)) < 0) {
		exit(EXIT_FAILURE);
	}

        make_folder_drakor("shift2/drakor");

        if ((chdir("shift2/drakor")) < 0) exit(EXIT_FAILURE);

	download_zip_drakor();

	unzip_drakor();

	make_kategori();

	move_file();

	list_data_drama("thriller");
	list_data_drama("horror");
	list_data_drama("action");
	list_data_drama("comedy");
	list_data_drama("romance");
	list_data_drama("school");
	list_data_drama("fantasy");

	remove_file();

	return 0;
}
